package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.model.Command;

public interface ICommandService {

    Command[] getAllCommands();

    String[] getTerminalCommands();

    String[] getArguments();

}