package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.api.IProjectService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isInvalidListIndex;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public int getSize() {
        return projectRepository.getSize();
    }

    @Override
    public boolean isEmpty() {
        return projectRepository.isEmpty();
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project add(final String name, final String description) {
        if (isEmptyString(name)) return null;
        final Project project = new Project(name, description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findById(final String id) {
        if (isEmptyString(id)) return null;
        return projectRepository.findById(id);
    }

    @Override
    public Project findByName(final String name) {
        if (isEmptyString(name)) return null;
        return projectRepository.findByName(name);
    }

    @Override
    public Project findByIndex(final int index) {
        if (isInvalidListIndex(index, projectRepository.getSize())) return null;
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project removeById(final String id) {
        if (isEmptyString(id)) return null;
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeByIndex(final int index) {
        if (isInvalidListIndex(index, projectRepository.getSize())) return null;
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project removeByName(final String name) {
        if (isEmptyString(name)) return null;
        return projectRepository.removeByName(name);
    }

    public Project updateProject(final Project project, final String name, final String description) {
        if (project == null || isEmptyString(name)) return null;
        projectRepository.update(project, name, description);
        return project;
    }

    @Override
    public Project updateByIndex(final int index, final String name, final String description) {
        if (isInvalidListIndex(index, projectRepository.getSize())) return null;
        return updateProject(findByIndex(index), name, description);
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (isEmptyString(id)) return null;
        return updateProject(findById(id), name, description);
    }

    @Override
    public Project updateStatus(Project project, Status status) {
        if (project == null || status == null) return null;
        projectRepository.updateStatus(project, status);
        return project;
    }

    @Override
    public Project startById(String id) {
        if (isEmptyString(id)) return null;
        return updateStatus(findById(id), Status.IN_PROGRESS);
    }

    @Override
    public Project startByIndex(int index) {
        if (isInvalidListIndex(index, projectRepository.getSize())) return null;
        return updateStatus(findByIndex(index), Status.IN_PROGRESS);
    }

    @Override
    public Project startByName(String name) {
        if (isEmptyString(name)) return null;
        return updateStatus(findByName(name), Status.IN_PROGRESS);
    }

    @Override
    public Project completeById(String id) {
        if (isEmptyString(id)) return null;
        return updateStatus(findById(id), Status.COMPLETED);
    }

    @Override
    public Project completeByIndex(int index) {
        if (isInvalidListIndex(index, projectRepository.getSize())) return null;
        return updateStatus(findByIndex(index), Status.COMPLETED);
    }

    @Override
    public Project completeByName(String name) {
        if (isEmptyString(name)) return null;
        return updateStatus(findByName(name), Status.COMPLETED);
    }

}
