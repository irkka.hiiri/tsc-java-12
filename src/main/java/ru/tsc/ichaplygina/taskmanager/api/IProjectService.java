package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project findById(final String id);

    Project findByName(final String name);

    Project findByIndex(final int index);

    Project add(final String name, final String description);

    Project removeById(final String id);

    Project removeByIndex(final int index);

    Project removeByName(final String name);

    Project updateByIndex(final int index, final String name, final String description);

    Project updateById(final String id, final String name, final String description);

    void clear();

    int getSize();

    boolean isEmpty();

    Project updateStatus(final Project project, final Status status);

    Project startById(final String id);

    Project startByIndex(final int index);

    Project startByName(final String name);

    Project completeById(final String id);

    Project completeByIndex(final int index);

    Project completeByName(final String name);

}
