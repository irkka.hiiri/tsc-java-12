package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    Project findById(final String id);

    Project findByName(final String name);

    Project findByIndex(final int index);

    void add(final Project project);

    void remove(final Project project);

    void update(final Project project, final String name, final String description);

    Project removeById(final String id);

    Project removeByIndex(final int index);

    Project removeByName(final String name);

    void clear();

    int getSize();

    boolean isEmpty();

    void updateStatus(final Project project, final Status status);

}
