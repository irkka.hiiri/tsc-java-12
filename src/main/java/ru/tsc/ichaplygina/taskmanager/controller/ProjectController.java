package ru.tsc.ichaplygina.taskmanager.controller;

import ru.tsc.ichaplygina.taskmanager.api.IProjectController;
import ru.tsc.ichaplygina.taskmanager.api.IProjectService;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.util.TerminalUtil;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printListWithIndexes;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    private void showNotFound() {
        printLinesWithEmptyLine(PROJECT_NOT_FOUND);
    }

    private void showRemoveResult(final boolean isSuccess) {
        if (isSuccess) printLinesWithEmptyLine(PROJECT_REMOVED);
        else showNotFound();
    }

    private void showUpdateResult(final boolean isSuccess) {
        if (isSuccess) printLinesWithEmptyLine(PROJECT_UPDATED);
        else printLinesWithEmptyLine(PROJECT_UPDATE_ERROR);
    }

    @Override
    public void showList() {
        if (projectService.isEmpty()) {
            printLinesWithEmptyLine(NO_PROJECTS_FOUND);
            return;
        }
        printListWithIndexes(projectService.findAll());
    }

    @Override
    public void clear() {
        final int count = projectService.getSize();
        projectService.clear();
        printLinesWithEmptyLine(count + PROJECTS_CLEARED);
    }

    @Override
    public void create() {
        final String name = TerminalUtil.readLine(NAME_INPUT);
        final String description = TerminalUtil.readLine(DESCRIPTION_INPUT);
        if (projectService.add(name, description) == null) printLinesWithEmptyLine(PROJECT_CREATE_ERROR);
        else printLinesWithEmptyLine(PROJECT_CREATED);
    }

    public void showProject(final Project project) {
        if (project == null) showNotFound();
        else printLinesWithEmptyLine(project);
    }

    @Override
    public void showById() {
        final String id = TerminalUtil.readLine(ID_INPUT);
        final Project project = this.projectService.findById(id);
        showProject(project);
    }

    @Override
    public void showByIndex() {
        final int index = TerminalUtil.readNumber(INDEX_INPUT);
        final Project project = this.projectService.findByIndex(index - 1);
        showProject(project);
    }

    @Override
    public void showByName() {
        final String name = TerminalUtil.readLine(NAME_INPUT);
        final Project project = this.projectService.findByName(name);
        showProject(project);
    }

    @Override
    public void updateById() {
        final String id = TerminalUtil.readLine(ID_INPUT);
        if (this.projectService.findById(id) == null) {
            showNotFound();
            return;
        }
        final String name = TerminalUtil.readLine(NAME_INPUT);
        final String description = TerminalUtil.readLine(DESCRIPTION_INPUT);
        final boolean isSuccess = this.projectService.updateById(id, name, description) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void updateByIndex() {
        final int index = TerminalUtil.readNumber(INDEX_INPUT);
        if (this.projectService.findByIndex(index - 1) == null) {
            showNotFound();
            return;
        }
        final String name = TerminalUtil.readLine(NAME_INPUT);
        final String description = TerminalUtil.readLine(DESCRIPTION_INPUT);
        final boolean isSuccess = this.projectService.updateByIndex(index - 1, name, description) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void removeById() {
        final String id = TerminalUtil.readLine(ID_INPUT);
        final boolean isSuccess = this.projectService.removeById(id) != null;
        showRemoveResult(isSuccess);
    }

    @Override
    public void removeByName() {
        final String name = TerminalUtil.readLine(NAME_INPUT);
        final boolean isSuccess = this.projectService.removeByName(name) != null;
        showRemoveResult(isSuccess);
    }

    @Override
    public void removeByIndex() {
        final int index = TerminalUtil.readNumber(INDEX_INPUT);
        final boolean isSuccess = this.projectService.removeByIndex(index - 1) != null;
        showRemoveResult(isSuccess);
    }

    @Override
    public void startByName() {
        final String name = TerminalUtil.readLine(NAME_INPUT);
        final boolean isSuccess = this.projectService.startByName(name) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void startById() {
        final String id = TerminalUtil.readLine(ID_INPUT);
        final boolean isSuccess = this.projectService.startById(id) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void startByIndex() {
        final int index = TerminalUtil.readNumber(INDEX_INPUT);
        final boolean isSuccess = this.projectService.startByIndex(index - 1) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void completeByName() {
        final String name = TerminalUtil.readLine(NAME_INPUT);
        final boolean isSuccess = this.projectService.completeByName(name) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void completeById() {
        final String id = TerminalUtil.readLine(ID_INPUT);
        final boolean isSuccess = this.projectService.completeById(id) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void completeByIndex() {
        final int index = TerminalUtil.readNumber(INDEX_INPUT);
        final boolean isSuccess = this.projectService.completeByIndex(index - 1) != null;
        showUpdateResult(isSuccess);
    }

}
