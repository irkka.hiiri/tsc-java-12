package ru.tsc.ichaplygina.taskmanager.api;

public interface IProjectController {

    void showList();

    void clear();

    void create();

    void showById();

    void showByIndex();

    void showByName();

    void updateById();

    void updateByIndex();

    void removeById();

    void removeByName();

    void removeByIndex();

    void startByIndex();

    void startById();

    void startByName();

    void completeByIndex();

    void completeById();

    void completeByName();

}
