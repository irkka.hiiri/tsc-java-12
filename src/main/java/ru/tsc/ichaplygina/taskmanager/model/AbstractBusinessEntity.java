package ru.tsc.ichaplygina.taskmanager.model;

import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.util.NumberUtil;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;

public abstract class AbstractBusinessEntity {

    private final String id = NumberUtil.generateId();

    private String name;

    private String description;

    private Status status = Status.PLANNED;

    public AbstractBusinessEntity() {
        this.name = EMPTY;
        this.description = EMPTY;
    }

    public AbstractBusinessEntity(final String name) {
        this.name = name != null ? name : EMPTY;
    }

    public AbstractBusinessEntity(final String name, final String description) {
        this.name = name != null ? name : EMPTY;
        this.description = description != null ? description : EMPTY;
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return this.id
                + DELIMITER
                + (!name.equals(EMPTY) ? name : PLACEHOLDER)
                + DELIMITER
                + (!description.equals(EMPTY) ? description : PLACEHOLDER)
                + DELIMITER
                + status;
    }

}
