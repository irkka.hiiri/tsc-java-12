package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.api.ITaskService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isInvalidListIndex;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public int getSize() {
        return taskRepository.getSize();
    }

    @Override
    public boolean isEmpty() {
        return taskRepository.isEmpty();
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task add(final String name, final String description) {
        if (isEmptyString(name)) return null;
        final Task task = new Task(name, description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findById(final String id) {
        if (isEmptyString(id)) return null;
        return taskRepository.findById(id);
    }

    @Override
    public Task findByName(final String name) {
        if (isEmptyString(name)) return null;
        return taskRepository.findByName(name);
    }

    @Override
    public Task findByIndex(final int index) {
        if (isInvalidListIndex(index, taskRepository.getSize())) return null;
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task removeById(final String id) {
        if (isEmptyString(id)) return null;
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final int index) {
        if (isInvalidListIndex(index, taskRepository.getSize())) return null;
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task removeByName(final String name) {
        if (isEmptyString(name)) return null;
        return taskRepository.removeByName(name);
    }

    public Task updateTask(final Task task, final String name, final String description) {
        if (task == null || isEmptyString(name)) return null;
        taskRepository.update(task, name, description);
        return task;
    }

    @Override
    public Task updateByIndex(final int index, final String name, final String description) {
        if (isInvalidListIndex(index, taskRepository.getSize())) return null;
        return updateTask(findByIndex(index), name, description);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (isEmptyString(id)) return null;
        return updateTask(findById(id), name, description);
    }

    @Override
    public Task updateStatus(Task task, Status status) {
        if (task == null || status == null) return null;
        taskRepository.updateStatus(task, status);
        return task;
    }

    @Override
    public Task startById(String id) {
        if (isEmptyString(id)) return null;
        return updateStatus(findById(id), Status.IN_PROGRESS);
    }

    @Override
    public Task startByIndex(int index) {
        if (isInvalidListIndex(index, taskRepository.getSize())) return null;
        return updateStatus(findByIndex(index), Status.IN_PROGRESS);
    }

    @Override
    public Task startByName(String name) {
        if (isEmptyString(name)) return null;
        return updateStatus(findByName(name), Status.IN_PROGRESS);
    }

    @Override
    public Task completeById(String id) {
        if (isEmptyString(id)) return null;
        return updateStatus(findById(id), Status.COMPLETED);
    }

    @Override
    public Task completeByIndex(int index) {
        if (isInvalidListIndex(index, taskRepository.getSize())) return null;
        return updateStatus(findByIndex(index), Status.COMPLETED);
    }

    @Override
    public Task completeByName(String name) {
        if (isEmptyString(name)) return null;
        return updateStatus(findByName(name), Status.COMPLETED);
    }

}
