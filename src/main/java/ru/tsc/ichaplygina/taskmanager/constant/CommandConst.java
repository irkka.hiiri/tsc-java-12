package ru.tsc.ichaplygina.taskmanager.constant;

public class CommandConst {

    public static final String CMD_VERSION = "version";

    public static final String CMD_ABOUT = "about";

    public static final String CMD_HELP = "help";

    public static final String CMD_INFO = "info";

    public static final String CMD_EXIT = "exit";

    public static final String CMD_LIST_COMMANDS = "list commands";

    public static final String CMD_LIST_ARGUMENTS = "list arguments";

    public static final String TASKS_LIST = "list tasks";

    public static final String TASKS_CLEAR = "clear tasks";

    public static final String TASK_CREATE = "create task";

    public static final String TASK_SHOW_BY_ID = "show task by id";

    public static final String TASK_SHOW_BY_INDEX = "show task by index";

    public static final String TASK_SHOW_BY_NAME = "show task by name";

    public static final String TASK_UPDATE_BY_INDEX = "update task by index";

    public static final String TASK_UPDATE_BY_ID = "update task by id";

    public static final String TASK_REMOVE_BY_INDEX = "remove task by index";

    public static final String TASK_REMOVE_BY_ID = "remove task by id";

    public static final String TASK_REMOVE_BY_NAME = "remove task by name";

    public static final String TASK_START_BY_INDEX = "start task by index";

    public static final String TASK_START_BY_ID = "start task by id";

    public static final String TASK_START_BY_NAME = "start task by name";

    public static final String TASK_COMPLETE_BY_INDEX = "complete task by index";

    public static final String TASK_COMPLETE_BY_ID = "complete task by id";

    public static final String TASK_COMPLETE_BY_NAME = "complete task by name";

    public static final String PROJECTS_LIST = "list projects";

    public static final String PROJECTS_CLEAR = "clear projects";

    public static final String PROJECT_CREATE = "create project";

    public static final String PROJECT_SHOW_BY_ID = "show project by id";

    public static final String PROJECT_SHOW_BY_INDEX = "show project by index";

    public static final String PROJECT_SHOW_BY_NAME = "show project by name";

    public static final String PROJECT_UPDATE_BY_INDEX = "update project by index";

    public static final String PROJECT_UPDATE_BY_ID = "update project by id";

    public static final String PROJECT_REMOVE_BY_INDEX = "remove project by index";

    public static final String PROJECT_REMOVE_BY_ID = "remove project by id";

    public static final String PROJECT_REMOVE_BY_NAME = "remove project by name";

    public static final String PROJECT_START_BY_INDEX = "start project by index";

    public static final String PROJECT_START_BY_ID = "start project by id";

    public static final String PROJECT_START_BY_NAME = "start project by name";

    public static final String PROJECT_COMPLETE_BY_INDEX = "complete project by index";

    public static final String PROJECT_COMPLETE_BY_ID = "complete project by id";

    public static final String PROJECT_COMPLETE_BY_NAME = "complete project by name";

}
