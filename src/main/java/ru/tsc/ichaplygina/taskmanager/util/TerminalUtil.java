package ru.tsc.ichaplygina.taskmanager.util;

import java.util.List;
import java.util.Scanner;

public final class TerminalUtil {

    private TerminalUtil() {
    }

    private final static Scanner SCANNER = new Scanner(System.in);

    public static String readLine() {
        return SCANNER.nextLine().trim();
    }

    public static String readLine(String output) {
        System.out.print(output);
        return readLine();
    }

    public static int readNumber() {
        try {
            return Integer.parseInt(readLine());
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    public static int readNumber(String output) {
        System.out.print(output);
        return readNumber();
    }

    public static void printLinesWithEmptyLine(final Object... lines) {
        System.out.println();
        for (Object line : lines) System.out.println(line);
        System.out.println();
    }

    public static void printListWithIndexes(final List list) {
        if (list == null) return;
        System.out.println();
        int index = 1;
        for (Object item : list) {
            System.out.println(index + ". " + item);
            index++;
        }
        System.out.println();
    }

}
