package ru.tsc.ichaplygina.taskmanager.controller;

import ru.tsc.ichaplygina.taskmanager.api.ITaskController;
import ru.tsc.ichaplygina.taskmanager.api.ITaskService;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.util.TerminalUtil;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printListWithIndexes;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    private void showNotFound() {
        printLinesWithEmptyLine(TASK_NOT_FOUND);
    }

    private void showRemoveResult(final boolean isSuccess) {
        if (isSuccess) printLinesWithEmptyLine(TASK_REMOVED);
        else showNotFound();
    }

    private void showUpdateResult(final boolean isSuccess) {
        if (isSuccess) printLinesWithEmptyLine(TASK_UPDATED);
        else printLinesWithEmptyLine(TASK_UPDATE_ERROR);
    }

    @Override
    public void showList() {
        if (taskService.isEmpty()) {
            printLinesWithEmptyLine(NO_TASKS_FOUND);
            return;
        }
        printListWithIndexes(taskService.findAll());
    }

    @Override
    public void clear() {
        final int count = taskService.getSize();
        taskService.clear();
        printLinesWithEmptyLine(count + TASKS_CLEARED);
    }

    @Override
    public void create() {
        final String name = TerminalUtil.readLine(NAME_INPUT);
        final String description = TerminalUtil.readLine(DESCRIPTION_INPUT);
        if (taskService.add(name, description) == null) printLinesWithEmptyLine(TASK_CREATE_ERROR);
        else printLinesWithEmptyLine(TASK_CREATED);
    }

    public void showTask(final Task task) {
        if (task == null) showNotFound();
        else printLinesWithEmptyLine(task);
    }

    @Override
    public void showById() {
        final String id = TerminalUtil.readLine(ID_INPUT);
        final Task task = this.taskService.findById(id);
        showTask(task);
    }

    @Override
    public void showByIndex() {
        final int index = TerminalUtil.readNumber(INDEX_INPUT);
        final Task task = this.taskService.findByIndex(index - 1);
        showTask(task);
    }

    @Override
    public void showByName() {
        final String name = TerminalUtil.readLine(NAME_INPUT);
        final Task task = this.taskService.findByName(name);
        showTask(task);
    }

    @Override
    public void updateById() {
        final String id = TerminalUtil.readLine(ID_INPUT);
        if (this.taskService.findById(id) == null) {
            showNotFound();
            return;
        }
        final String name = TerminalUtil.readLine(NAME_INPUT);
        final String description = TerminalUtil.readLine(DESCRIPTION_INPUT);
        final boolean isSuccess = this.taskService.updateById(id, name, description) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void updateByIndex() {
        final int index = TerminalUtil.readNumber(INDEX_INPUT);
        if (this.taskService.findByIndex(index - 1) == null) {
            showNotFound();
            return;
        }
        final String name = TerminalUtil.readLine(NAME_INPUT);
        final String description = TerminalUtil.readLine(DESCRIPTION_INPUT);
        final boolean isSuccess = this.taskService.updateByIndex(index - 1, name, description) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void removeById() {
        final String id = TerminalUtil.readLine(ID_INPUT);
        final boolean isSuccess = this.taskService.removeById(id) != null;
        showRemoveResult(isSuccess);
    }

    @Override
    public void removeByName() {
        final String name = TerminalUtil.readLine(NAME_INPUT);
        final boolean isSuccess = this.taskService.removeByName(name) != null;
        showRemoveResult(isSuccess);
    }

    @Override
    public void removeByIndex() {
        final int index = TerminalUtil.readNumber(INDEX_INPUT);
        final boolean isSuccess = this.taskService.removeByIndex(index - 1) != null;
        showRemoveResult(isSuccess);
    }

    @Override
    public void startByName() {
        final String name = TerminalUtil.readLine(NAME_INPUT);
        final boolean isSuccess = this.taskService.startByName(name) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void startById() {
        final String id = TerminalUtil.readLine(ID_INPUT);
        final boolean isSuccess = this.taskService.startById(id) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void startByIndex() {
        final int index = TerminalUtil.readNumber(INDEX_INPUT);
        final boolean isSuccess = this.taskService.startByIndex(index - 1) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void completeByName() {
        final String name = TerminalUtil.readLine(NAME_INPUT);
        final boolean isSuccess = this.taskService.completeByName(name) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void completeById() {
        final String id = TerminalUtil.readLine(ID_INPUT);
        final boolean isSuccess = this.taskService.completeById(id) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void completeByIndex() {
        final int index = TerminalUtil.readNumber(INDEX_INPUT);
        final boolean isSuccess = this.taskService.completeByIndex(index - 1) != null;
        showUpdateResult(isSuccess);
    }

}
