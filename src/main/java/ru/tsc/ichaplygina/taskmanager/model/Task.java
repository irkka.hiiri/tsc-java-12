package ru.tsc.ichaplygina.taskmanager.model;

public class Task extends AbstractBusinessEntity {

    public Task() {
    }

    public Task(String name) {
        super(name);
    }

    public Task(String name, String description) {
        super(name, description);
    }

}
